﻿namespace BMap
{
    public class BaiduMapContext
    {
        public BaiduMapContext(string mapID, string fixedMapID, double latitude, double longitude, int zoomlevel)
        {
            MapID = mapID;
            Latitude = latitude;
            Longitude = longitude;
            Zoom = zoomlevel;
            FixedMapID = fixedMapID;
        }

        public string MapID { get; private set; }

        public string FixedMapID { get; private set; }

        public double Latitude { get; private set; }

        public double Longitude { get; private set; }

        public int Zoom { get; private set; }
    }
}