﻿using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BMap
{
    [ParseChildren(true)]
    public class BaiduMap : WebControl
    {
        [Bindable(true)]
        [Themeable(false)]
        [Category("Map Data")]
        public double Latitude
        {
            get
            {
                var obj = ViewState["Map_Latitude"];
                if (obj == null)
                {
                    return 0;
                }
                return (double) obj;
            }
            set { ViewState["Map_Latitude"] = value; }
        }

        [Bindable(true)]
        [Themeable(false)]
        [Category("Map Data")]
        public double Longitude
        {
            get
            {
                var obj = ViewState["Map_Longitude"];
                if (obj == null)
                {
                    return 0;
                }
                return (double)obj;
            }
            set { ViewState["Map_Longitude"] = value; }
        }

        [Bindable(true)]
        [Themeable(false)]
        [Category("Map Data")]
        public int Zoom
        {
            get
            {
                var obj = ViewState["Map_Zoom"];
                if (obj == null)
                {
                    return 2;
                }
                return (int)obj;
            }
            set { ViewState["Map_Zoom"] = value; }
        }

        [Bindable(false)]
        [Themeable(false)]
        [Category("Map Data")]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        [MergableProperty(false)]
        public MapCollection<MapControl> MapControls { get; private set; }

        [Bindable(false)]
        [Themeable(false)]
        [Category("Map Data")]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        [MergableProperty(false)]
        public MapCollection<MapMarker> Markers { get; private set; }

        public BaiduMap()
        {
            MapControls = new MapCollection<MapControl>();
            Markers = new MapCollection<MapMarker>();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.Write("<div");
            writer.WriteAttribute("id", ClientID);
            if (!string.IsNullOrEmpty(CssClass))
            {
                writer.WriteAttribute("class", CssClass);
            }
            if (Width != Unit.Empty)
            {
                Style.Add("width", Width.ToString());
            }
            if (Height != Unit.Empty)
            {
                Style.Add("height", Height.ToString());
            }
            if (!string.IsNullOrEmpty(Style.Value))
            {
                writer.WriteAttribute("style", Style.Value);
            }
            writer.Write("></div>\r\n");
            writer.RenderJsInclude(ClientID);
            writer.FuncBegin();
            writer.InitMap(ClientID, Latitude, Longitude, Zoom);
            writer.RenderMapControl(ClientID,  MapControls.ToArray());
            writer.RenderMapMarker(ClientID, Markers.ToArray());
            writer.FuncEnd();
        }
    }
}
