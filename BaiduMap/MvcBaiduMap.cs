﻿using System;
using System.Web.Mvc;

namespace BMap
{
    public class MvcBaiduMap : IDisposable
    {
        private readonly ViewContext _viewContext;
        private bool _disposed;

        public string MapID { get; private set; }

        public MvcBaiduMap(ViewContext viewContext, string mapId)
        {
            if (viewContext == null)
            {
                throw  new ArgumentNullException("viewContext");
            }
            _viewContext = viewContext;
            MapID = mapId;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            _disposed = true;
            BaiduExtension.EndMap(_viewContext);
        }
    }
}