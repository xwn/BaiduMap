﻿namespace BMap
{
    public class MapLabelOption
    {
        public string Text { get; set; }

        public double PointX { get; set; }

        public double PointY { get; set; }

        public double OffsetX { get; set; }

        public double OffsetY { get; set; }

        public object Style { get; set; }
    }
}