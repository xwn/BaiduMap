﻿namespace BMap
{
    public class MapControl
    {
        public MapControlType ControlType { get; set; }

        public int OptionX
        {
            get { return Option == null ? 0 : Option.X; }
            set
            {
                if (Option == null)
                {
                    Option = new MapControlOption();
                }
                Option.X = value;
            }
        }

        public int OptionY
        {
            get { return Option == null ? 0 : Option.Y; }
            set
            {
                if (Option == null)
                {
                    Option = new MapControlOption();
                }
                Option.Y = value;
            }
        }

        public string OptionType
        {
            get { return Option == null ? null : Option.Type; }
            set
            {
                if (Option == null)
                {
                    Option = new MapControlOption();
                }
                Option.Type = value;
            }
        }

        public MapAnchorEnum OptionAnchor
        {
            get { return Option == null ? MapAnchorEnum.None : Option.Anchor; }
            set
            {
                if (Option == null)
                {
                    Option = new MapControlOption();
                }
                Option.Anchor = value;
            }
        }

        public MapControlOption Option { get; set; }
    }
}
