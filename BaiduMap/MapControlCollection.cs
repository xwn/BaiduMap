﻿using System;
using System.Collections;
using System.Linq;

namespace BMap
{
    public class MapCollection<T> : IList where T: class
    {
        private readonly ArrayList _mapControls;

        public MapCollection()
        {
            _mapControls = new ArrayList();
        }

        public IEnumerator GetEnumerator()
        {
            return _mapControls.GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            _mapControls.CopyTo(array, index);
        }

        public int Count
        {
            get { return _mapControls.Count; }
        }

        public object SyncRoot
        {
            get { return this; }
        }

        public bool IsSynchronized
        {
            get { return _mapControls.IsSynchronized; }
        }

        public void Add(T marker)
        {
            _mapControls.Add(marker);
        }

        int IList.Add(object value)
        {
             return _mapControls.Add(value);
        }

        public bool Contains(object value)
        {
            return _mapControls.Contains(value);
        }

        public void Clear()
        {
            _mapControls.Clear();
        }

        public int IndexOf(object value)
        {
            return _mapControls.IndexOf(value);
        }

        public void Insert(int index, object value)
        {
            _mapControls.Insert(index, value);
        }

        public void Remove(object value)
        {
            _mapControls.Remove(value);
        }

        public void RemoveAt(int index)
        {
            _mapControls.RemoveAt(index);
        }

        public T this[int index]
        {
            get { return (T) _mapControls[index]; }
        }

        object IList.this[int index]
        {
            get { return _mapControls[index]; }
            set { _mapControls[index] = value; }
        }

        public bool IsReadOnly
        {
            get { return _mapControls.IsReadOnly; }
        }

        public bool IsFixedSize
        {
            get { return false; }
        }

        public T[] ToArray()
        {
            return this.Cast<T>().ToArray();
        }
    }
}