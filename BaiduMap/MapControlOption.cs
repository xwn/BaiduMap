﻿namespace BMap
{
    public class MapControlOption
    {
        public MapAnchorEnum Anchor { get; set; }

        public string Type { get; set; }

        public int X { get; set; }

        public int Y { get; set; }
    }
}