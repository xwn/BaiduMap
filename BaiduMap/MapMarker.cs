﻿namespace BMap
{
    public class MapMarker
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string IconUrl { get; set; }
        public string IconWidth { get; set; }
        public string IconHeight { get; set; }
        public string Animation { get; set; }
        public bool Draggable { get; set; }
        public string OnClientClick { get; set; }
    }
}
