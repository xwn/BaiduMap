﻿namespace BMap
{
    public enum MapControlType
    {
        NavigationControl,
        OverviewMapControl,
        ScaleControl,
        MapTypeControl,
        CopyrightControl,
        GeolocationControl,
    }
}
