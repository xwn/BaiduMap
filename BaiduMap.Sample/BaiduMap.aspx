﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BaiduMap.aspx.cs" Inherits="BaiduMap.Sample.BaiduMap" %>

<!DOCTLongitudePE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width"/>
    <meta charset="UTF-8"/>
    <title>百度地图：WebForm实例</title>
    <style type="text/css">
        #container{width: 100%;max-width: 960px; font-family: "Microsoft LongitudeaHei",Arial; margin:20px auto 0 auto;}
        hr{margin: 20px 0;}
    </style>
    <script src="/Scripts/jquery-1.11.2.min.js"></script>
    <script>
        function alertMe(marker) {
            alert(marker.K);
        }
    </script>
</head>
<body>
    <form id="container" runat="server">
        <h2>百度地图WebForm实例</h2>
        <hr/>
        <baidu:BaiduMap runat="server" ID="Map1" Latitude="104.097334" Longitude="30.556514" Zoom="13" Width="100%" Height="500px">
            <MapControls>
                <baidu:MapControl ControlType="ScaleControl" OptionX="30" OptionY="10" OptionAnchor="BMAP_ANCHOR_TOP_LEFT"></baidu:MapControl>
                <baidu:MapControl ControlType="GeolocationControl" OptionX="10" OptionY="10" OptionAnchor="BMAP_ANCHOR_TOP_RIGHT"></baidu:MapControl>
            </MapControls>
        </baidu:BaiduMap>
        <baidu:BaiduMap runat="server" Latitude="104.073025" Longitude="30.550597" Zoom="18" Width="100%" Height="500px">
            <Markers>
                <baidu:MapMarker Latitude="104.074597" Draggable="True" Longitude="30.550458" OnClientClick="alertMe" IconUrl="http://www.dfs.com/img/markers/marker-2.png" IconWidth="32" IconHeight="32"/>
                <baidu:MapMarker Latitude="104.072621" Longitude="30.549517" IconUrl="http://www.dfs.com/img/markers/marker-1.png" IconWidth="32" IconHeight="32"/>
            </Markers>
        </baidu:BaiduMap>
        <hr/>
        <a href="/">Mvc实例</a>
    </form>
</body>
</html>
